package com.webinar.firstapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class FirstActivity extends Activity {

    private TextView myTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);

        myTextView = (TextView) findViewById(R.id.textView);
        changeTextView("Czesc Jan");

    }

    private void changeTextView(String newText) {
        myTextView.setText(newText);
    }

    public void doSomething(View view) {
        EditText inputText = (EditText) findViewById(R.id.editText);
        String input = inputText.getText().toString();
//        changeTextView("Czesc " + input);
//        myTextView.setTextColor(getResources().getColor(R.color.red));
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("param", input);
        startActivity(intent);
    }
}
