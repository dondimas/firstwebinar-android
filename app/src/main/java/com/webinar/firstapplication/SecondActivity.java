package com.webinar.firstapplication;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by dmitry on 14/08/14.
 */
public class SecondActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        String param = getIntent().getExtras().getString("param");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(param);
    }
}
